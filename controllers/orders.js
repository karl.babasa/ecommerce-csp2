//[SECTION] Dependencies and Modules

	const Order = require('../models/Order');
	const User = require('../models/User');

//[SECTION] Functionalities [CREATE]

	//[SUB-SECTION] [ADD TO CART]
	
		module.exports.addToCart = async (userDetails, result, req, res) => {

			//PRODUCT TO USER

				let isUserUpdated = await User.findById(userDetails.id).then(user => {

					let newProduct = {
						productId: result.id,
						productName: result.name,
						//productCategory: result.category[0].categoryId,
						ProductQuantity: req.body.quantity,
						productPrice: result.price,
						productTotalPrice: req.body.quantity * result.price/*,
						voucherDiscount: 0,
						productDiscountedPrice: (req.body.quantity * result.price) - 50*/
					}


					user.cart.push(newProduct);
					return user.save().then(order => true).catch(err => err.message)		
							
				})

				if(isUserUpdated !== true) {
					res.send({message: isUserUpdated})
				}

				if(isUserUpdated) {
					
					res.send({message: `${req.body.quantity} ${result.name} added to your Cart`})
				}

		}

	//[SUB-SECTION] [CHECKOUT]

		module.exports.checkout = async (userData, i, savedOrder, totalAmountToPay, req, res) => {
		
			let productId = userData.cart[i].productId;
			let productName = userData.cart[i].productName;
			let productQuantity = userData.cart[i].ProductQuantity;
			let productTotalPrice = userData.cart[i].productTotalPrice;
			let userId = userData.id;
			let userName = userData.firstName + userData.lastName;
			//let voucherDiscount = userData.cart[i].voucherDiscount;
			//let productFinalPrice = userData.cart[i].productPrice*userData.cart[i].productPrice;

			let isOrderUpdated = await Order.findById(savedOrder.id).then(order => {

				let checkoutCart = {
					productId: productId,
					productName: productName,
					productQuantity: productQuantity,
					productTotalPrice: productTotalPrice/*,
					voucherDiscount: voucherDiscount,
					productDiscountedPrice: productTotalPrice - voucherDiscount*/
				}
				
				order.product.push(checkoutCart);
				return order.save().then(user => true).catch(err => err.message)

			})

			if(isOrderUpdated !== true) {
					res.send({message: isOrderUpdated})
				}

			let isUserUpdated = await User.findById(userData.id).then(user => {

				let userOrder= {
					orderId: savedOrder.id,
					productId: productId,
					productName: productName,
					productQuantity: productQuantity,
					productTotalPrice: productTotalPrice/*,
					voucherDiscount: voucherDiscount,
					productDiscountedPrice: productTotalPrice - voucherDiscount*/
				}

				user.order.push(userOrder);
				return user.save().then(user => true).catch(err => err.message)

			})

			if(isUserUpdated !== true) {
					res.send({message: isUserUpdated})
				}
				for (let a = userData.cart.length + 1; a >= 0; a--) {

				let isRemove = await User.findById(userData.id).then(user => {

				let removeCart = {
					productId: productId,
					productName: productName,
					productCategory: userData.cart[i].productCategory,
					ProductQuantity: productQuantity,
					productPrice: userData.cart[i].productPrice,
					productTotalPrice: productTotalPrice,
					_id: userData.cart[i].id
				}

					user.cart.pull(removeCart);
				
				
				return user.save().then(user => true).catch(err => err.message)

			})

				}

			if(isUserUpdated !== true) {
					res.send({message: isUserUpdated})
				}

		}


//[SECTION] Functionalities [RETRIEVE]

	//[SUB-SECTION] [GET-ALL ORDERS]

		module.exports.getAllOrders = () => {

			return Order.find({}).then(result => {
				return result;
			});
		};

	//[SUB-SECTION] [GET SINGLE ORDER]

		module.exports.getOrder = (id) => {
			
			return Order.findById(id).then(resultOfQuery => {
				return resultOfQuery;
			})
		};

//[SECTION] Functionalities [UPDATE]

	/*module.exports.attachVoucher = (userData, i, voucherId, req, res) => {

		if (userData.cart[i].productCategory == voucherId) {
			let discount = (userData.cart[i].productTotalPrice * userData.voucher[0].voucherDiscount)/100
			console.log(discount)
		}


	}*/

//[SECTION] Functionalities [DELETE]