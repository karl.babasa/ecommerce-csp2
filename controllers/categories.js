//[SECTION] Dependencies and Modules

	const Product = require('../models/Product');
	const Category = require('../models/Category');

//[SECTION] Functionalities [CREATE]

	//[SUB-SECTION] [CREATE CATEGORY]

		module.exports.createCategory = (info) => {
			let categoryName = info.name
		
			let newCategory = new Category ({
				name: categoryName,
			});
		
			return newCategory.save().then((savedCategory, err) => {
				if (err) {
					return `Failed to add new Category`
				} else {
					return savedCategory;
				}
			});
		};

	//[SUB-SECTION] [CATEGORIZED PRODUCT]

		module.exports.categorizeProduct = async (productId, categoryDetails, result, res) => {

			let isProductUpdated = await Product.findById(result.id).then(product => {
				
				let newCategory = {
					categoryId: categoryDetails.categoryId,
					categoryName: categoryDetails.categoryName
				}

				product.category.push(newCategory)
				return product.save().then(category => true).catch(err => err.message)
			})

			if(isProductUpdated !== true) {
				return res.send({message: isProductUpdated})
			}

			let isCategoryUpdated = await Category.findById(categoryDetails.categoryId).then(category => {

				let newProduct = {

					productId: result.id,
					productName: result.name,
					isAvailable: result.isAvailable,
					productPrice: result.price
				}

				category.product.push(newProduct)
				return category.save().then(product => true).catch(err => err.message)
			})

			if(isProductUpdated !== true) {
				return res.send({message: isProductUpdated})
			}

			if (isProductUpdated && isCategoryUpdated) {
				return res.send(`Successfully Categorized a Product`)
			}

		}


//[SECTION] Functionalities [RETRIEVE]

	//[SUB-SECTION] [GET-ALL CATEGORIES]

		module.exports.getAllCategory = () => {

			return Category.find({}).then(result => {
				return result;
			});
		};

	//[SUB-SECTION] [GET SINGLE CATEGORY]

		module.exports.getCategory = (id) => {
			
			return Category.findById(id).then(resultOfQuery => {
				return resultOfQuery;
			})
		};

//[SECTION] Functionalities [UPDATE]

	//[SUB-SECTION] [UPDATE CATEGORY]

		module.exports.updateCategory = (id, details) => {
		
			let categoryName = details.name;
			
			let updatedCategory = {
				name: categoryName
			}
			
			return Category.findByIdAndUpdate(id, updatedCategory).then((categoryUpdated, err) => {
				
				if (err) {
					return 'Failed to update Category';
				} else {
					return 'Successfully Updated Category';
				}
			})
		}

//[SECTION] Functionalities [DELETE]