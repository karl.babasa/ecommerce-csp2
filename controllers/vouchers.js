//[SECTION] Dependencies and Modules
	
	const Voucher = require("../models/Voucher");
	const User = require("../models/User");

	const auth = require("../auth");

//[SECTION] Functionalities [CREATE]

	//[SUB-SECTION] [ADDING VOUCHER]

		module.exports.createVoucher = (info) => {

		let voucherName = info.name;
		let vouchertDescription = info.description;
		let voucherDiscount = info.discount;
		let isPercentageDiscount = info.isPercentageDiscount;
		let categoryId = info.categoryId;
		let voucherStock = info.stock;

		let newVoucher = new Voucher ({
			name: voucherName,
			description: vouchertDescription,
			discount: voucherDiscount,
			isPercentageDiscount: isPercentageDiscount,
			categoryId: categoryId,
			stock: voucherStock
		});

		return newVoucher.save().then((savedVoucher, err) => {
				if (err) {
					return 'Failed to Add New Voucher'
				} else {
					return savedVoucher;
				}
			});
	 
		};

	//[SUB-SECTION] [CLAIM VOUCHER] 

		module.exports.claimVoucher = async (id, userDetails, result, res) => {

			//[CLAIMING VOUCHER]

			let isUserUpdated = await User.findById(userDetails.id).then(user => {

				let newAddedVoucer = {
					voucherId: result.id,
					voucherName: result.name,
					voucherDescription: result.description,
					voucherDiscount: result.discount,
					isPercentageDiscount: result.isPercentageDiscount,
					categoryId: result.categoryId
				}

				user.voucher.push(newAddedVoucer);
				return user.save().then(voucher => true).catch(err => err.message)
			})

			if(isUserUpdated !== true) {
				return ress.send({message: isUserUpdated})
			}

			//[USER TO VOUCHER]

			let isVoucherUpdated = await Voucher.findById(result.id).then(voucher => {

				let userVoucher = {
					userId: userDetails.id,
					userName: `${userDetails.firstName} ${userDetails.lastName}`,
					email: userDetails.email,
					voucherQuantity: userDetails.stock
				}

				voucher.user.push(userVoucher);

				return voucher.save().then(course => true).catch(err => err.message)
			})

			if(isVoucherUpdated !== true) {
				return res.send({message: isVoucherUpdated})
			}

			if(isUserUpdated && isVoucherUpdated) {
				
				res.send(`${userDetails.firstName} ${userDetails.lastName} successfully claimed a voucher: ${result.name}`)
			}

		}



//[SECTION] Functionalities [RETRIEVE]

	//[SUB-SECTION] [GET-ALL VOUCHER]

		module.exports.getAllVoucher = () => {

			return Voucher.find({}).then(result => {
				return result;
			});
		};

	//[SUB-SECTION] [GET SINGLE PRODUCT]

		module.exports.getVoucher = (id) => {
			
			return Voucher.findById(id).then(resultOfQuery => {
				return resultOfQuery;
			})
		};

	//[SUB-SECTION] [GET ALL ACTIVE ACTIVE]

		module.exports.getAllActiveVoucher = () => {
			
			return Voucher.find({isAvailable: true}).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		};

//[SECTION] Functionalities [UPDATE]

	//[SUB-SECTION] [DEACTIVATE PRODUCT] [SOFT DELETE]

		module.exports.deactivateVoucher = (id) => {
			
			let updates = {
				isAvailable: false
			}
			
			return Voucher.findByIdAndUpdate(id, updates).then((archived, err) => {
				
				if (archived) {
					return `The Voucher ${id} is now unavailable`;
				} else {
					return `Failed to remove Voucher`
				}
			})
		}

		//[SUB-SECTION] [ACTIVATE PRODUCT] [RESTORE]

		module.exports.activateVoucher = (id) => {
			
			let updates = {
				isAvailable: true
			}
			
			return Voucher.findByIdAndUpdate(id, updates).then((archived, err) => {
				
				if (archived) {
					return `The Voucher ${id} is now Available`;
				} else {
					return `Failed to restore Voucher`
				}
			})
		}

		//[SUB-SECTION] [ADD STOCK TO VOUCHER] 

		module.exports.addStock = (id, details, result) => {

			let addedStock = details.stock;
			let oldStock = result.stock;
			let newStack = addedStock + oldStock

			let updates = {
				stock: newStack,
				isAvailable: true
			}

			return Voucher.findByIdAndUpdate(id, updates).then((stockAdded, err) => {
				if (stockAdded) {
					return `${result.name}'s total stock are now ${newStack}`;
				} else {
					return `Failed to add stock`
				}
			})

		}

//[SECTION] Functionalities [DELETE]