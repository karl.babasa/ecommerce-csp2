//[SECTION] Dependencies and Modules
	
	const mongoose = require("mongoose");

//[SECTION] Schema / Document Blueprint
	
	const voucherSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Voucher Name is Required"]
		},
		description: {
			type: String,
			required: [true, "Voucher Description is Required"]
		},
		discount: {
			type: Number,
			required: [true, "Discount is Required"]
		},
		isPercentageDiscount: {
			type: Boolean,
			default: false
		},
		categoryId: {
			type: String,
			default: "0001"
		},
		stock: {
			type: Number,
			required: [true, "Please enter the initial stock of the voucher"]
		},
		isAvailable: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date
		},
		user: [{
			userId: {
				type: String
			},
			userName: {
				type: String
			},
			email: {
				type: String
			},
			voucherQuantity: {
				type: String
			}
		}]

	})
//[SECTION] Model
	
	const Voucher = mongoose.model("Voucher", voucherSchema);
	module.exports = Voucher;