//[SECTION] Dependencies and Modules
	
	const mongoose = require("mongoose");

//[SECTION] Schema / Document Blueprint
	
	const categorySchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Category name is Required"]
		},
		product: [{
			productId: {
				type: String
			},
			productName: {
				type: String
			},
			isAvailable: {
				type: Boolean
			},
			productPrice: {
				type: String
			}
		}]

	});

//[SECTION] Model
	
	const Category = mongoose.model("Category", categorySchema);
	module.exports = Category;