//[SECTION] Dependencies and Modules
	
	const mongoose = require("mongoose");

//[SECTION] Schema / Document Blueprint
	
	const orderSchema = new mongoose.Schema({
		user:[{
			userId:{
				type: String,
				required: true
			},
			userName: {
				type: String,
				required: true
			}
		}],
		product:[{
			productId: {
				type: String,
				required: true
			},
			productName: {
				type: String,
				required: true
			},
			productQuantity: {
				type: Number,
				required: true
			},
			productTotalPrice: {
				type: Number,
				required: true
			},
			voucherDiscount: {
				type: Number,
				default: 0
			},
			productDiscountedPrice: {
				type: Number,
				//required: true
			}
		}],
		finalPrice: {
			type: Number,
			//required: true
		},
		purchasedOn:{
			type: Date,
			default: new Date
		},
		IsDelivered: {
			type: Boolean,
			default: false
		}
	});

//[SECTION] Model
	
	const Order = mongoose.model("Order", orderSchema);
	module.exports = Order;