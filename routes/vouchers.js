//[SECTION] Dependencies and Modules
	
	const exp = require("express");
	const controller = require('../controllers/vouchers');
	const controllerProduct = require('../controllers/products');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

	const route = exp.Router();

//[SECTION] [POST] Routes

	//[SUB-SECTION] [CREATE VOUCHER]

		route.post('/create', (req, res) => {

			let data = req.body;

			if (data.stock < 1) {
				return res.send('Add Stocks')
			}
			if (data.isPercentageDiscount === true && data.discount > 100) {

				return res.send(`invalid Discount`)

			} else {

				controller.createVoucher(data).then(outcome => {
					res.send(outcome);

					});
			}
		});

	//[SUB-SECTION] [CLAIM VOUCHER] 

		route.post('/:id/claim', verify, (req, res) => {
			let voucherId = req.params.id;
			let userDetails = req.user;
			
			controller.getVoucher(voucherId).then(result => {

					controller.claimVoucher(voucherId, userDetails, result, res).then(resultOfTheFunction => {
						
						res.send(resultOfTheFunction);

					})
			})
		});

//[SECTION] [GET] Routes

	//[SUB-SECTION] [GET-ALL] Route

		route.get('/all', (req, res) => {
			controller.getAllVoucher().then(outcome => {
				res.send(outcome);
			});
		});

	//[SUB-SECTION] [GET SINGLE PRODUCT]
	
		route.get('/:id', verify, (req, res) => {
			let voucherId = req.params.id;

			controller.getVoucher(voucherId).then(result => {
				console.log(result)
				res.send(result);


			})
		})

	//[SUB-SECTION] [GET ALL ACTIVE PRODUCTS]
	
		route.get('/', (req, res) => {
			
			controller.getAllActiveVoucher().then(outcome => {
				
				res.send(outcome);
			});
		});

//[SECTION] [PUT] Routes

	//[SUB-SECTION] [DEACTIVATE VOUCHER] [SOFT DELETE]

		route.put('/:id/archive', (req, res) => {
			
			let voucherId = req.params.id;
			
			controller.deactivateVoucher(voucherId).then(resultOfTheFunction => {
				
				res.send(resultOfTheFunction);
			})
		});

	//[SUB-SECTION] [ACTIVATE VOUCHER] [RESTORE]

		route.put('/:id/restore', (req, res) => {
			
			let voucherId = req.params.id;
			
			controller.activateVoucher(voucherId).then(resultOfTheFunction => {
				
				res.send(resultOfTheFunction);
			})
		});

	//[SUB-SECTION] [ADD STOCK TO VOUCHER] 

		route.put('/:id/stock', (req, res) => {
			let voucherId = req.params.id;
			let details = req.body;
			let addStock = details.stock;

			controller.getVoucher(voucherId).then(result => {

				if (addStock >= 1) {

					controller.addStock(voucherId, details, result).then(resultOfTheFunction => {
						res.send(resultOfTheFunction);
					})
				} else {
					res.send(`Use positive numbers`)
				}
			})
		});

//[SECTION] [DELETE] Routes

//[SECTION] Export Route System

	module.exports = route;