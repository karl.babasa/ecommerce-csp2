//[SECTION] Dependencies and Modules

	const exp = require("express");
	const controller = require('../controllers/categories');
	const controllerProduct = require('../controllers/products');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

	const route = exp.Router();

//[SECTION] [POST] Routes

	//[SUB-SECTION] [CREATE CATEGORY]

		route.post('/create', (req, res) => {

			let data = req.body;
			controller.createCategory(data).then(outcome => {
				res.send(outcome);
			});
		});

	//[SUB-SECTION] [CATEGORIZED]

		route.post('/:id/categorize', async (req, res) => {

			let productId = req.params.id;
			let categoryDetails = req.body;
			

			controllerProduct.getProduct(productId).then(result => {
	
				controller.categorizeProduct(productId, categoryDetails, result, res).then(outcome => {
					
				})

			})
		})

//[SECTION] [GET] Routes

	//[SUB-SECTION] [GET-ALL] Route

		route.get('/all', (req, res) => {
			controller.getAllCategory().then(outcome => {
				res.send(outcome);
			});
		});

		//[SUB-SECTION] [SINGLE CATEGORY]

		route.get("/:id", (req, res) => {
			let productId = req.params.id;

			controller.getCategory(productId).then(result => {
				res.send(result)
			})
		});

//[SECTION] [PUT] Routes

	//[SUB-SECTION] [UPDATE PRODUCT]

		route.put('/:id', (req, res) => {

			let id = req.params.id;
			let details = req.body;

			let categoryName = details.name;
			
			if (categoryName !== '') {

				controller.updateCategory(id, details).then(outcome => {
				res.send(outcome);
			});

			} else {
				res.send('Incorrect Input, Make sure details are complete')
			}

		});

//[SECTION] [DELETE] Routes

//[SECTION] Export Route System

	module.exports = route;