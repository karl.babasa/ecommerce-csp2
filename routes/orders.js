//[SECTION] Dependencies and Modules

	const exp = require("express");
	const controller = require('../controllers/orders');
	const controllerProduct = require('../controllers/products');
	const controllerUser = require('../controllers/users');
	const Order = require('../models/Order');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;


//[SECTION] Routing Component

	const route = exp.Router();

//[SECTION] [POST] Routes

	//[SUB-SECTION] [ADD TO CART]

		route.post('/:id/cart', verify, (req, res) => {

			if (req.user.isAdmin == true || req.user.isAdminAssistant == true) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let productId = req.params.id;
			let userDetails = req.user;

			
			controllerProduct.getProduct(productId).then(result => {

				if (req.body.quantity > result.stock) {

						return res.send({message: `Only ${result.stock} ${result.name} left: Not enough Stock`})	
					}

				controller.addToCart(userDetails, result, req, res).then(resultOfTheFunction => {
						
					return res.send(resultOfTheFunction);

				})
			})
		});

	//[SUB-SECTION] [CHECKOUT]


		route.post('/checkout',verify, (req,res) => {

			if (req.user.isAdmin == true || req.user.isAdminAssistant == true) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let userDetails = req.user;
			let totalAmountToPay = 0
				
				controllerUser.getUserData(userDetails.id).then(userData => {

					for (let i = userData.cart.length - 1; i >= 0; i--) {
						totalAmountToPay = totalAmountToPay + userData.cart[i].productTotalPrice
						
					}
					let newOrder = new Order ({
						user: [{
							userId: userDetails.id,
							userName: `${req.user.firstName} ${req.user.lastName}`
						}],

						finalPrice: totalAmountToPay
					})

					 newOrder.save().then((savedOrder, err) => {

					for (let i = userData.cart.length - 1; i >= 0; i--) {
						
						controller.checkout(userData, i, savedOrder, totalAmountToPay, req, res).then(resultOfTheFunction => {
						
						})
						
						
					}

					res.send({message: `Thank you for Purchasing`})
				})

			})
		})


//[SECTION] [GET] Routes

	//[SUB-SECTION] [GET-ALL] Route

		route.get('/all', verify, verifyAdmin, (req, res) => {
			controller.getAllOrders().then(outcome => {
				res.send(outcome);
			});
		});

	//[SUB-SECTION] [GET SINGLE ORDER]
	
		route.get('/:id', verify, (req, res) => {
			let productId = req.params.id;

			controller.getOrder(productId).then(result => {
				return res.send(result);


			})
		})

//[SECTION] [PUT] Routes
	/*
	route.put('/:id/voucher', verify, (req, res) => {
		let userDetails = req.user;
		let voucherId = req.params.id;
		console.log(userDetails.id)

		controllerUser.getUserData(userDetails.id).then(userData => {
		
			for (let i = userData.cart.length - 1; i >= 0; i--) {

				controller.attachVoucher(userData, i, voucherId, res, req).then(resultOfTheFunction => {

				})
			}
		})
	})*/

//[SECTION] [DELETE] Routes

//[SECTION] Export Route System

	module.exports = route;
